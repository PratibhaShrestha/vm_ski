-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 18, 2018 at 11:26 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vm_ski`
--
CREATE DATABASE IF NOT EXISTS `vm_ski` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `vm_ski`;

-- --------------------------------------------------------

--
-- Table structure for table `Ovelse`
--

DROP TABLE IF EXISTS `Ovelse`;
CREATE TABLE IF NOT EXISTS `Ovelse` (
  `OvelseNr` int(11) NOT NULL AUTO_INCREMENT,
  `Type` varchar(20) NOT NULL,
  `Sted` varchar(50) NOT NULL,
  `Dato` date NOT NULL,
  `Tid` time NOT NULL,
  PRIMARY KEY (`OvelseNr`)
) ENGINE=InnoDB AUTO_INCREMENT=332212 DEFAULT CHARSET=latin1;

--
-- RELATIONSHIPS FOR TABLE `Ovelse`:
--

--
-- Dumping data for table `Ovelse`
--

INSERT INTO `Ovelse` (`OvelseNr`, `Type`, `Sted`, `Dato`, `Tid`) VALUES
(223344, 'Curling', 'Bane 5 - Skinnskattberget', '2018-03-30', '12:02:00'),
(332211, 'Biathlon', 'Bane 2 - Porthøgda', '2018-03-20', '21:22:00');

-- --------------------------------------------------------

--
-- Table structure for table `Person`
--

DROP TABLE IF EXISTS `Person`;
CREATE TABLE IF NOT EXISTS `Person` (
  `Personnr` bigint(12) NOT NULL,
  `Fornavn` varchar(50) NOT NULL,
  `Etternavn` varchar(50) NOT NULL,
  `Adresse` varchar(50) NOT NULL,
  `PostNr` int(4) NOT NULL,
  `Poststed` varchar(20) NOT NULL,
  `Telefonnr` int(11) NOT NULL,
  `Land` varchar(50) NOT NULL,
  PRIMARY KEY (`Personnr`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONSHIPS FOR TABLE `Person`:
--

--
-- Dumping data for table `Person`
--

INSERT INTO `Person` (`Personnr`, `Fornavn`, `Etternavn`, `Adresse`, `PostNr`, `Poststed`, `Telefonnr`, `Land`) VALUES
(1122334455, 'Test', 'User', 'TestAdddress', 1234, 'Oslo', 112233445, 'North Korea'),
(3099023073, 'Sijan', 'test', 'Testing Address', 955, 'Oslo', 96847064, 'Norway'),
(11122233344, 'Pratibha', 'Shrestha', 'RÃ¸dtvetveien 35', 955, 'Oslo', 93991129, 'Afghanistan');

-- --------------------------------------------------------

--
-- Table structure for table `Publikum`
--

DROP TABLE IF EXISTS `Publikum`;
CREATE TABLE IF NOT EXISTS `Publikum` (
  `PublikumNr` int(11) NOT NULL AUTO_INCREMENT,
  `Personnr` bigint(12) NOT NULL,
  `Billetttype` varchar(20) NOT NULL,
  PRIMARY KEY (`PublikumNr`),
  KEY `PK` (`Personnr`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- RELATIONSHIPS FOR TABLE `Publikum`:
--   `Personnr`
--       `Person` -> `Personnr`
--

--
-- Dumping data for table `Publikum`
--

INSERT INTO `Publikum` (`PublikumNr`, `Personnr`, `Billetttype`) VALUES
(4, 11122233344, 'Platinum - 1000 Kr');

-- --------------------------------------------------------

--
-- Table structure for table `Registrasjon`
--

DROP TABLE IF EXISTS `Registrasjon`;
CREATE TABLE IF NOT EXISTS `Registrasjon` (
  `RegNr` int(10) NOT NULL,
  `PersonNr` bigint(12) NOT NULL,
  `OvelseNr` int(10) NOT NULL,
  `Type` varchar(10) NOT NULL,
  PRIMARY KEY (`RegNr`),
  KEY `OK` (`OvelseNr`),
  KEY `RK` (`PersonNr`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONSHIPS FOR TABLE `Registrasjon`:
--   `OvelseNr`
--       `Ovelse` -> `OvelseNr`
--   `PersonNr`
--       `Person` -> `Personnr`
--

-- --------------------------------------------------------

--
-- Table structure for table `Utover`
--

DROP TABLE IF EXISTS `Utover`;
CREATE TABLE IF NOT EXISTS `Utover` (
  `UtoverNr` int(11) NOT NULL AUTO_INCREMENT,
  `Personnr` bigint(12) NOT NULL,
  `Nasjonalitet` varchar(20) NOT NULL,
  PRIMARY KEY (`UtoverNr`),
  KEY `UtpK` (`Personnr`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- RELATIONSHIPS FOR TABLE `Utover`:
--   `Personnr`
--       `Person` -> `Personnr`
--

--
-- Dumping data for table `Utover`
--

INSERT INTO `Utover` (`UtoverNr`, `Personnr`, `Nasjonalitet`) VALUES
(3, 3099023073, 'Norway');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Publikum`
--
ALTER TABLE `Publikum`
  ADD CONSTRAINT `PuPk` FOREIGN KEY (`Personnr`) REFERENCES `Person` (`Personnr`);

--
-- Constraints for table `Registrasjon`
--
ALTER TABLE `Registrasjon`
  ADD CONSTRAINT `OK` FOREIGN KEY (`OvelseNr`) REFERENCES `Ovelse` (`OvelseNr`),
  ADD CONSTRAINT `RK` FOREIGN KEY (`PersonNr`) REFERENCES `Person` (`Personnr`);

--
-- Constraints for table `Utover`
--
ALTER TABLE `Utover`
  ADD CONSTRAINT `UtpK` FOREIGN KEY (`Personnr`) REFERENCES `Person` (`Personnr`);
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
