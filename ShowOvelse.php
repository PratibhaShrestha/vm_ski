<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Show Personer</title>
    <link rel="shortcut icon" href="../../favicon.ico">
	<link rel="stylesheet" type="text/css" href="css/normalize.css" />
	<link rel="stylesheet" type="text/css" href="css/demo.css" />
	<link rel="stylesheet" type="text/css" href="css/icons.css" />
    <link rel="stylesheet" type="text/css" href="css/style2.css" />

    <script src="js/modernizr.custom.js"></script>
    <script type="text/javascript" src="./forms/view.js"></script>
	<link rel="stylesheet" type="text/css" href="./forms/view.css" media="all">
    <link rel="stylesheet" type="text/css" href="style.css" media="all">
    <script defer src="fonts/solid.js" integrity="sha384-+Ga2s7YBbhOD6nie0DzrZpJes+b2K1xkpKxTFFcx59QmVPaSA8c7pycsNaFwUK6l" crossorigin="anonymous"></script>
    <script defer src="fonts/fontawesome.js" integrity="sha384-7ox8Q2yzO/uWircfojVuCQOZl+ZZBg2D2J5nkpLqzH1HY0C1dHlTKIbpRz/LG23c" crossorigin="anonymous"></script>

</head>

<body id="main_body">
<div id="show_container">
			<div class="form_description">
                <p></p>
				<h2 class="show_title"><i class="fas fa-calendar fa-2x"></i> &nbsp;Alle Øvelser liste ...</h2>
				<p></p>
            </div>

        <table class="show_table">
            <tr>
            <th>Øvelse-Nummer</th>
            <th>Type</th>
            <th>Sted</th>
            <th>Dato</th>
            <th>Tid</th>
            <th>Action</th>
            </tr>

            <?php
            session_start();
            
$db = new mysqli("localhost", "root", "", "vm_ski");
$db->set_charset("utf8");
if (isset($_GET['ovelsenr']) && $_GET['action'] == "delete") {    
    $overlseNr = $_GET['ovelsenr'];
    echo "<label style='color:red'>Ovelsenr: $overlseNr </label>";
    $sql = "delete from Registrasjon where OvelseNr= $overlseNr";
    $resultat = $db->query($sql);
    if (!$resultat) {
        echo "<label style='color:red'>Feil: $db->error</label>";
    } else {
        $sql = "delete from Ovelse where OvelseNr= $overlseNr";
        $resultat = $db->query($sql);
        if (!$resultat) {
            echo "<label style='color:red'>Feil: $db->error</label>";
        } else {
            echo "<label style='color:green'>Success deleting of OvelseNr: $overlseNr .</label>";
        }
        header("Location: ShowOvelse.php");
    }
}

$sql = "select * from Ovelse";
$resultat = $db->query($sql);
if (!$resultat) {
    echo "<label style='color:red'>No records found</label>";
}
while ($rad = $resultat->fetch_array(MYSQLI_ASSOC)) {
    ?>
            <tr>
            <td><?echo $rad["OvelseNr"] ?></td>
            <td><?echo $rad["Type"] ?></td>
            <td><?echo $rad["Sted"] ?></td>
            <td><?echo $rad["Dato"] ?></td>
            <td><?echo $rad["Tid"] ?></td>
            <?php
            //checking if user exists..
            if( isset($_SESSION["logged_in"]) and isset($_SESSION["user_name"]) ){
                echo '<td><a href="./forms/ovelse.php?action=edit&ovelsenr=' . $rad["OvelseNr"] . '"><i class="fas fa-edit"></i></a> _ ';
                echo "<a onClick=\"javascript: return confirm('Please confirm deletion');\" href='ShowOvelse.php?action=delete&ovelsenr=". $rad['OvelseNr'] ."'><i class='fas fa-trash-alt'></i></a></td>";
			}else{
                echo "<td></td>";
            }
            ?>
            <a href="ShowOvelse.php"></a>
            </td>
            </tr>
            <?php
}
$db->close();
?>
</table>
</div>
<nav id="bt-menu" class="bt-menu">
				<a href="#" class="bt-menu-trigger"><span>Menu</span></a>
				<ul>
					<li><a href="./forms/person.php" class="bt-icon icon-user-outline"></a>Register Person</li>
					<li><a href="./forms/ovelse.php" class="bt-icon icon-flag" ></a>Register Øvelse</li>
					<li><a href="./forms/registration.php" class="bt-icon icon-file-add" ></a>Register Deltakelse</li>
					<li><a href="./ShowOvelse.php" class="bt-icon icon-calendar" ></a>Show Øvelse liste</li>
					<li><a href="./ShowPerson.php" class="bt-icon icon-browser" ></a>Show Person Liste</li>
					<li><a href="./ShowUtoverOvelsePublikum.php" class="bt-icon icon-browser" ></a>Show Øvelse-Deltagelse Liste</li>
				</ul>
			</nav>
</body>
<script src="./js/classie.js"></script>
<script src="./js/borderMenu.js"></script>
</html>