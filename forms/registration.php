<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Registering person til øvelser.</title>
	<link rel="shortcut icon" href="../../favicon.ico">
	<link rel="stylesheet" type="text/css" href="../css/normalize.css" />
	<link rel="stylesheet" type="text/css" href="../css/demo.css" />
	<link rel="stylesheet" type="text/css" href="../css/icons.css" />
	<link rel="stylesheet" type="text/css" href="../css/style2.css" />
	<script src="../js/modernizr.custom.js"></script>
	<script type="text/javascript" src="view.js"></script>
	<link rel="stylesheet" type="text/css" href="view.css" media="all">

</head>

<?php
require_once "../class/Person.php";
require_once "../class/Ovelse.php";

$person = new Person();
$ovelse = new Ovelse();
$personList = $person->getPersonList();
$ovelseList = $ovelse->getOvelseList();
?>

<?php
require_once "../class/Registrasjon.php";
require_once "../class/Ovelse.php";
require_once "../class/Person.php";
require_once "../class/Utover.php";
require_once "../class/Publikum.php";

if (isset($_REQUEST["Register"]) && $_REQUEST["Register"] == "Register") {

    // Getting the Registration info into classes...
    $registrasjon = new Registrasjon();
    $registrasjon->setRegnr($_POST["regnr"]);

    //getting Nr for oversetype.. (ovelsetype)
    $ovelseType = $_POST["ovelsetype"];
    $ovelseNr = Ovelse::getOvelseNrFromType($ovelseType);
    $registrasjon->setOvelsenr($ovelseNr);

    //Getting NR for personnavn ...
    $personNavn = $_POST["personnavn"];
    $personObject = Person::getPersonFromNavn($personNavn);
    $registrasjon->setPersonnr($personObject->getPersonNr());

    $regType = $_POST["regType"];
    $registrasjon->setType($regType);

    //Billetttype if type is Publikum
    if ($regType == "Publikum") {
        $publikum = new Publikum();
        $publikum->setPersonnr($personObject->getPersonNr());
        $publikum->setBilletttype($_POST["billettType"]);
        $publikum->insertIntoDatabase();
    } else if ($regType == "Utøver") {
        $utover = new Utover();
        $utover->setPersonnr($personObject->getPersonNr());
        $utover->setNasjonalitet($personObject->getLand());
        $utover->insertIntoDatabase();
    }
    $registrasjon->insertIntoDatabase();
}
?>
<?php session_start(); ?>
<body id="main_body">
	<?php
			$isLoggedIn = $_SESSION["logged_in"];
			$user_name = $_SESSION["user_name"];
			if(empty($user_name) or $isLoggedIn != 1){
				echo "Not logged in, you must be logged in to register!";
				header("Location: ./../login.php"); /* Redirect browser */
				exit();
			}else{
				echo "<h2>Welcome back, $user_name !!!</h2>";
			}
	?>

	<img id="top" src="top.png" alt="">
	<div id="form_container">

		<h1>
			<a>Registering person til øvelser.</a>
		</h1>
		<form id="form_1434" class="appnitro" method="post" action="registration.php">
			<div class="form_description">
				<h2>Registering person til øvelser.</h2>
				<p></p>
			</div>
			<ul>

				<li id="li_1">
					<label class="description" for="regnr">Registrasjon nummer </label>
					<div>
						<input id="element_1" name="regnr" class="element text small" type="text" maxlength="255" value="" />
					</div>
				</li>
				<li id="li_3">
					<label class="description" for="ovelsetype">Velg Øvelse </label>
					<div>
						<select class="element select medium" id="element_3" name="ovelsetype">
							<option value="" selected="selected">Velg hvilken øvelse</option>
							<?php
foreach ($ovelseList as $ovelse) {
    echo "<option value='$ovelse'>$ovelse</option>";
}?>
						</select>
					</div>
				</li>
				<li id="li_4">
					<label class="description" for="personnavn">Velg Person
					</label>
					<div>
						<select class="element select medium" id="element_4" name="personnavn">
							<option value="" selected="selected">--Velg person--</option>
							<?php
foreach ($personList as $person) {
    echo "<option value='$person'>$person</option>";
}?>
						</select>
					</div>
				</li>

				<script type="text/javascript">
					function showSelected(val){
						if( val === "Publikum"){
							document.getElementById('li_6').style.display = "block";
						}else{
							document.getElementById('li_6').style.display = "none";
						}
					}
				</script>

				<li id="li_5">
					<label class="description" for="regType" id="selectedResult">Registere person som: </label>
					<div>
						<select class="element select medium" id="element_5" name="regType" onChange="showSelected(this.value)">
							<option value="" selected="selected">--Velg deltakelse type--</option>
							<option value="Utøver">Utøver</option>
							<option value="Publikum">Publikum</option>
						</select>
					</div>
				</li>

				<li id="li_6" style="display:none">
					<label class="description" for="billettType">Billett-type </label>
					<div>
						<select class="element select medium" id="element_6" name="billettType">
							<option value="" selected="selected">--Velg billett--</option>
							<option value="Platinum - 1000 Kr">Platinum - 1000 Kr</option>
							<option value="Gold - 800 Kr">Gold - 800 Kr</option>
							<option value="Silver - 500 Kr">Silver - 500 Kr</option>
							<option value="Bronze - 300 Kr">Bronze - 300 Kr</option>
						</select>
					</div>
				</li>

				<li class="buttons">
					<input type="hidden" name="form_id" value="1434" />
					<input id="saveForm" class="button_text" type="submit" name="Register" value="Register" />
				</li>
			</ul>
		</form>
		<nav id="bt-menu" class="bt-menu">
				<a href="#" class="bt-menu-trigger"><span>Menu</span></a>
				<ul>
					<li><a href="../index.php" class="bt-icon icon-home"></a>Home</li>
					<li><a href="./person.php" class="bt-icon icon-user-outline"></a>Register Person</li>
					<li><a href="./ovelse.php" class="bt-icon icon-flag"></a>Register Øvelse</li>
					<li><a href="../ShowOvelse.php" class="bt-icon icon-calendar"></a>Show Øvelse liste</li>
					<li><a href="../ShowPerson.php" class="bt-icon icon-browser"></a>Show Person Liste</li>
					<li><a href="..ShowUtoverOvelsePublikum.php" class="bt-icon icon-browser" ></a>Show Øvelse-Deltagelse Liste</li>
				</ul>
			</nav>
		<div id="footer">
			Generated by
			<a href="http://www.phpform.org">pForm</a>
		</div>
	</div>
	<img id="bottom" src="bottom.png" alt="">
</body>
<script src="../js/classie.js"></script>
<script src="../js/borderMenu.js"></script>
</html>