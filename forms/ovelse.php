<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Øvelse Registrasjon</title>
	<link rel="shortcut icon" href="../../favicon.ico">
	<link rel="stylesheet" type="text/css" href="../css/normalize.css" />
	<link rel="stylesheet" type="text/css" href="../css/demo.css" />
	<link rel="stylesheet" type="text/css" href="../css/icons.css" />
	<link rel="stylesheet" type="text/css" href="../css/style2.css" />
	<script src="../js/modernizr.custom.js"></script>
	<script type="text/javascript" src="view.js"></script>
	<link rel="stylesheet" type="text/css" href="view.css" media="all">
	<script type="text/javascript" src="calendar.js"></script>
</head>


<?php
// This is for ADDING...
require_once "../class/Ovelse.php";

if (isset($_REQUEST["Register"]) && ($_REQUEST["Register"] == "Register" || $_REQUEST["Register"] == "Update")) {
    // Getting the ovelse info into classes...
    $ovelse = new Ovelse;
    $ovelse->setOvelsenr($_POST["ovelsenr"]);
    $ovelse->setType($_POST["type"]);
    $ovelse->setSted($_POST["sted"]);

    //Date format:  YYYY-MM-DD
    $datoVal = $_POST["element_2_3"] . "-" . $_POST["element_2_2"] . "-" . $_POST["element_2_1"];
    $ovelse->setDato($datoVal);

    // time Format: HH:MM AM/PM
    $tidVal = $_POST["hour"] . ":" . $_POST["minute"] . " " . $_POST["ampm"];
    $ovelse->setTid($tidVal);

    if ($_REQUEST["Register"] == "Register") {
        $ovelse->insertIntoDatabase();
    } else if ($_REQUEST["Register"] == "Update") {
        $ovelse->updateDatabase();
    }

}
?>
<?php
//Default values..
$radObject = null;
//For Date and time to be separated .... - and : respectively
$datoVals = null;
$tidVals = null;
// WHEN WE GET THE EDITING ACTION REQUEST
if (isset($_GET['ovelsenr']) && $_GET['action'] == "edit") {
    $db = new mysqli("localhost", "root", "", "vm_ski");
    $overlseNr = $_GET['ovelsenr'];
    $sql = "Select * from Ovelse where OvelseNr= $overlseNr";
    $editResult = $db->query($sql);
    if ($editResult) {
        $antallRader = $db->affected_rows;
        if ($antallRader > 0) {
            $radObject = $editResult->fetch_object();
            //For Date and time to be separated .... - and : respectively
            $datoVals = explode("-", ($radObject->Dato));
            $tidVals = explode(":", ($radObject->Tid));

            echo "* Type seleted was : " . $radObject->Type . "<br/>";
            echo "* Sted / Bane seleted was : " . $radObject->Sted . "<br/>";

            echo '<script type="text/javascript">',
            'SelectElement("$radObject->Type")',
                '</script>';

            $showRegister = false;

        }
    } else {
        echo ("SQL: " . $sql . ", Databasefeil i Ovelse: " . $db->error);
    }
}
?>

<script>
	function SelectElement(valueToSelect)
	{
    var element = document.getElementById('element_4');
    element.value = valueToSelect;
	}
</script>
<?php session_start(); ?>
<body id="main_body">
	<?php
			$isLoggedIn = $_SESSION["logged_in"];
			$user_name = $_SESSION["user_name"];
			if(empty($user_name) or $isLoggedIn != 1){
				echo "Not logged in, you must be logged in to register!";
				header("Location: ./../login.php"); /* Redirect browser */
				exit();
			}else{
				echo "<h2>Welcome back, $user_name !!!</h2>";
			}
	?>

	<img id="top" src="top.png" alt="">
	<div id="form_container">

		<h1>
			<a>Øvelse Registrasjon</a>
		</h1>
		<form id="form_1434" class="appnitro" method="post" action="ovelse.php">
			<div class="form_description">
				<h2>Øvelse Registrasjon</h2>
				<p></p>
			</div>
			<ul>

				<li id="li_1">
					<label class="description" for="element_1">Øvelse nummer </label>
					<div>
						<input id="element_1" name="ovelsenr" class="element text small" type="text" maxlength="255" value="<?php echo (isset($radObject)) ? $radObject->OvelseNr : ''; ?>"
						<?php echo isset($showRegister) ? 'disabled' : ''; ?>/>
					</div>
				</li>
				<li id="li_4">
					<label class="description" for="type">Type </label>
					<div>
					<input id="element_1" name="type" class="element text small" type="text" maxlength="255" value="<?php echo (isset($radObject)) ? $radObject->Type : ''; ?>"
						<?php echo isset($showRegister) ? 'disabled' : ''; ?>/>
					</div>
					<p class="guidelines" id="guide_4">
						<small>
						</small>
					</p>
				</li>
				<li id="li_5">
					<label class="description" for="sted">Sted/ Bane </label>
					<div>
						<select class="element select medium" id="element_5" name="sted">
							<option value="" selected="selected">--velg bane--</option>
							<option value="Bane 1 - Kjerkeberget">Bane 1 - Kjerkeberget</option>
							<option value="Bane 2 - Porthøgda">Bane 2 - Porthøgda</option>
							<option value="Bane 3 - Katnosfjellet">Bane 3 - Katnosfjellet</option>
							<option value="Bane 4 - Heikampen">Bane 4 - Heikampen</option>
							<option value="Bane 5 - Skinnskattberget">Bane 5 - Skinnskattberget</option>

						</select>
					</div>
				</li>
				<li id="li_2">
					<label class="description" for="element_2">Velg Dato </label>
					<span>
						<input id="element_2_1" name="element_2_1" class="element text" size="2" maxlength="2" value="<?php echo isset($datoVals) ? $datoVals[0] : ''; ?>" type="text"> /
						<label for="element_2_1">DD</label>
					</span>
					<span>
						<input id="element_2_2" name="element_2_2" class="element text" size="2" maxlength="2" value="<?php echo isset($datoVals) ? $datoVals[1] : ''; ?>" type="text"> /
						<label for="element_2_2">MM</label>
					</span>
					<span>
						<input id="element_2_3" name="element_2_3" class="element text" size="4" maxlength="4" value="<?php echo isset($datoVals) ? $datoVals[2] : ''; ?>" type="text">
						<label for="element_2_3">YYYY</label>
					</span>

					<span id="calendar_2">
						<img id="cal_img_2" class="datepicker" src="calendar.gif" alt="Pick a date.">
					</span>
					<script type="text/javascript">
						Calendar.setup({
							inputField: "element_2_3",
							baseField: "element_2",
							displayArea: "calendar_2",
							button: "cal_img_2",
							ifFormat: "%B %e, %Y",
							onSelect: selectEuropeDate
						});
					</script>

				</li>
				<li id="li_3">
					<label class="description" for="element_3">Velg Tid </label>
					<span>
						<input id="element_3_1" name="hour" class="element text " size="2" type="text" maxlength="2" value="<?php echo isset($tidVals) ? $tidVals[0] : ''; ?>" /> :
						<label>HH</label>
					</span>
					<span>
						<input id="element_3_2" name="minute" class="element text " size="2" type="text" maxlength="2" value="<?php echo isset($tidVals) ? $tidVals[1] : ''; ?>" /> :
						<label>MM</label>
					</span>
					<span>
						<select class="element select" style="width:4em" id="element_3_4" name="ampm">
							<option value="AM">AM</option>
							<option value="PM">PM</option>
						</select>
						<label>AM/PM</label>
					</span>
				</li>

				<li class="buttons">
					<input type="hidden" name="form_id" value="1434" />
					<input style="<?php echo isset($showRegister) ? 'display:none' : 'display:block'; ?>" id="registerForm" class="button_text" type="submit" name="Register" value="Register" />
					<input style="<?php echo isset($showRegister) ? 'display:block' : 'display:none'; ?>" id="updateForm" class="button_text" type="submit" name="Register" value="Update" />
				</li>
			</ul>
		</form>
		<nav id="bt-menu" class="bt-menu">
				<a href="#" class="bt-menu-trigger"><span>Menu</span></a>
				<ul>
					<li><a href="../index.php" class="bt-icon icon-home"></a>Home</li>
					<li><a href="./person.php" class="bt-icon icon-user-outline"></a>Register Person</li>
					<li><a href="./registration.php" class="bt-icon icon-file-add"></a>Register Deltakelse</li>
					<li><a href="../ShowOvelse.php" class="bt-icon icon-calendar"></a>Show Øvelse liste</li>
					<li><a href="../ShowPerson.php" class="bt-icon icon-browser"></a>Show Person Liste</li>
					<li><a href="../ShowUtoverOvelsePublikum.php" class="bt-icon icon-browser" ></a>Show Øvelse-Deltagelse Liste</li>
				</ul>
			</nav>
		<div id="footer">
			Generated by
			<a href="http://www.phpform.org">pForm</a>
		</div>
	</div>
	<img id="bottom" src="bottom.png" alt="">
</body>
<script src="../js/classie.js"></script>
<script src="../js/borderMenu.js"></script>
</html>