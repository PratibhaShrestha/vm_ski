<?php

session_start(); //to ensure you are using same session
$_SESSION["user_name"] ="";
$_SESSION["logged_in"] = 0;
session_destroy(); //destroy the session
header("location:index.php"); //to redirect back to "index.php" after logging out
exit();

?>