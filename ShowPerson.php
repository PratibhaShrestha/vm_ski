<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Show Personer</title>
    <link rel="shortcut icon" href="../../favicon.ico">
	<link rel="stylesheet" type="text/css" href="css/normalize.css" />
	<link rel="stylesheet" type="text/css" href="css/demo.css" />
	<link rel="stylesheet" type="text/css" href="css/icons.css" />
    <link rel="stylesheet" type="text/css" href="css/style2.css" />

    <script src="js/modernizr.custom.js"></script>
    <script type="text/javascript" src="./forms/view.js"></script>
	<link rel="stylesheet" type="text/css" href="./forms/view.css" media="all">
    <link rel="stylesheet" type="text/css" href="style.css" media="all">
    <script defer src="fonts/solid.js" integrity="sha384-+Ga2s7YBbhOD6nie0DzrZpJes+b2K1xkpKxTFFcx59QmVPaSA8c7pycsNaFwUK6l" crossorigin="anonymous"></script>
    <script defer src="fonts/fontawesome.js" integrity="sha384-7ox8Q2yzO/uWircfojVuCQOZl+ZZBg2D2J5nkpLqzH1HY0C1dHlTKIbpRz/LG23c" crossorigin="anonymous"></script>
</head>

<body id="main_body">
<div id="show_container">
			<div class="form_description">
                <p></p>
				<h2 class="show_title"><i class="fas fa-users fa-3x"></i>Alle personer liste ...</h2>
				<p></p>
	        </div>
        <table class="show_table">
            <tr>
            <th>Personnr</th>
            <th>Fornavn</th>
            <th>Etternavn</th>
            <th>Adresse</th>
            <th>Postnr</th>
            <th>Poststed</th>
            <th>Telefonnr</th>
            <th>Land</th>
            </tr>
            <?php
$db = mysqli_connect("localhost", "root", "", "vm_ski");
$db->set_charset("utf8");
$sql = "select * from Person";
$resultat = mysqli_query($db, $sql);
if (!$resultat) {
    echo "Feil med lesing fra database";
}
while ($rad = mysqli_fetch_array($resultat, MYSQLI_ASSOC)) {
    ?>
            <tr>
            <td><?echo $rad["Personnr"] ?></td>
            <td><?echo $rad["Fornavn"] ?></td>
            <td><?echo $rad["Etternavn"] ?></td>
            <td><?echo $rad["Adresse"] ?></td>
            <td><?echo $rad["PostNr"] ?></td>
            <td><?echo $rad["Poststed"] ?></td>
            <td><?echo $rad["Telefonnr"] ?></td>
            <td><?echo $rad["Land"] ?></td>
            </tr>
            <?php
}
mysqli_close($db);
?>
            </table>
</div>
<nav id="bt-menu" class="bt-menu">
				<a href="#" class="bt-menu-trigger"><span>Menu</span></a>
				<ul>
                    <li><a href="./index.php" class="bt-icon icon-home"></a>Home</li>
					<li><a href="./forms/person.php" class="bt-icon icon-user-outline"></a>Register Person</li>
					<li><a href="./forms/ovelse.php" class="bt-icon icon-flag"></a>Register Øvelse</li>
					<li><a href="./forms/registration.php" class="bt-icon icon-file-add"></a>Register Deltakelse</li>
					<li><a href="./ShowOvelse.php" class="bt-icon icon-calendar"></a>Show Øvelse liste</li>
					<li><a href="./ShowUtoverOvelsePublikum.php" class="bt-icon icon-browser" ></a>Show Øvelse-Deltagelse Liste</li>
				</ul>
			</nav>
</body>
<script src="./js/classie.js"></script>
<script src="./js/borderMenu.js"></script>
</html>