<link rel="stylesheet" href="./css/login.css">
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login dialog!</title>
</head>

<body>
        <?php
        function checkIfValueExists($brukernavn, $password)
        {
            $db = new mysqli("localhost", "root", "", "vm_ski");
            if (!$db) {
                printError("Kunne ikke koble til databasen, prøv igjen."); //denne initierer en die(), sjekk printError funksjonen lenger opp i denne filen
            }
            $sql = "SELECT * FROM Login where ";
            $sql .= "Brukernavn = '" . $brukernavn . "' and Passord = '" . md5($password) . "'";
            $result = $db->query($sql);
            if ($result->num_rows > 0) {
                session_start();
                $_SESSION["logged_in"] = true;
                $_SESSION["user_name"] = $brukernavn;
                header("Location: ./index.php"); /* Redirect browser */
            } else {
                echo "<div class='container' style='text-align:center;'>";
                echo "<label style='color:red;'>Feil brukernavn eller passord , prøv igjen!</label>";
                echo "</div>";
            }
            $db->close();
        }

        if (isset($_REQUEST["login"]) && $_REQUEST["login"] == "login") {
            $username = $_POST["username"];
            $password = $_POST["password"];

            checkIfValueExists($username, $password);
        }

        ?>
    <!-- Modal Content -->
    <div class="container" style="text-align:center;">
        <label>You must be logged in to register !</label>
    </div>
    <form class="modal-content" action="login.php" method="POST">
        <div class="imgcontainer">
            <img src="./images/img_avatar2.png" alt="Avatar" class="avatar">
        </div>

        <div class="container">
            <label for="uname">
                <b>Username</b>
            </label>
            <input type="text" placeholder="Enter Username" name="username" required>

            <label for="psw">
                <b>Password</b>
            </label>
            <input type="password" placeholder="Enter Password" name="password" required>

			<button type="submit" value="login" name="login">Login</button>

            <a class="cancelbtn" href="register.php" style="background-color:lightgreen;">Register</a>
    		<a class="cancelbtn" href="index.php">Cancel</a>
		</div>
    </form>
</body>
</html>