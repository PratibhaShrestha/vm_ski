<link rel="stylesheet" href="./css/login.css">
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login dialog!</title>

</head>

<body>
<?php
function insertIntoDatabase($brukernavn, $password, $navn)
{
    $db = new mysqli("localhost", "root", "", "vm_ski");
    if (!$db) {
        printError("Kunne ikke koble til databasen, prøv igjen."); //denne initierer en die(), sjekk printError funksjonen lenger opp i denne filen
    }
    $sql = "Insert into Login (Brukernavn, Passord, Navn) ";
    $sql .= "values('" . $brukernavn . "','" . md5($password) . "','" . $navn . "')";

    $result = $db->query($sql);
    if ($result) {
        echo "Bruker registrert";
    } else {
        echo ("SQL: " . $sql . ", Databasefeil i Register: " . $db->error);
    }
    $db->close();
}

function validateBrukerNavn(){
    $resultat = preg_match("/^([a-åA-Å]{2,20})*$/", $_POST["brukernavn"]);
    if ($resultat) {
        echo "Validering av brukernavn feltet er OK";
    } else {
        echo "Feil, brukernavn må bare inneholde store og små bokstaver og 2-20 siffre.";
    }
    return $resultat;
}
function validatePasswordField(){
    $resultat = preg_match("/^([a-åA-Å0-9]{4,12})*$/", $_POST["password"]);
    if ($resultat) {
        echo "Validering av password feltet er OK";
    } else {
        echo "<br/>>Feil, password må bare inneholde store og små bokstaver og siffer mellom 0 and 9";
    }
    return $resultat;
}
function validateFullNavn(){
    $resultat = preg_match("/^([a-åA-Å ]{3-50})*$/", $_POST["navn"]);
    if ($resultat) {
        echo "Validering av navn feltet er OK";
    } else {
        echo "<br/>Feil, navn må bare inneholde store og små bokstaver og mellomrom,og 3-50 siffre";
    }
    return $resultat;
}

if (isset($_REQUEST["register"]) && $_REQUEST["register"] == "Register") {
    $brukernavn = $_POST["brukernavn"];
    $password = $_POST["password"];
    $navn = $_POST["navn"];

    if( validateBrukerNavn() && validatePasswordField() && validateFullNavn()){
        insertIntoDatabase($brukernavn, $password, $navn);
    }else{
        echo "<br/>Fix alle input fields!!!";
    }
}
?>

    <?php
session_start();
try {
    $isLoggedIn = $_SESSION["logged_in"];
    $user_name = $_SESSION["user_name"];
    if (empty($user_name) or $isLoggedIn != 1) {
        // echo "Not logged in, you must be logged in to register!";
        header("Location: ./login.php"); /* Redirect browser */
        exit();
    } else {
        echo "<div class='container' style='text-align:center;'>";
        echo "<h2></h2>";
        echo "<label style='color:green;'>Welcome back, $user_name !!</label>";
        echo "</div>";

    }
} catch (Exception $e) {
    echo "Caught exception : $e->getMessage()";
}
?>

<!-- The Modal -->
<form class="modal-content" action="register.php" name="registerform" method="POST" style="border:1px solid #ccc">
    <div class="container">
      <h1>Sign Up</h1>
      <p>Please fill in this form to create an account.</p>
      <hr>

      <label for="brukernavn"><b>Brukernavn *</b></label>
      <div id = "feilbrukernavn" class="inputLabel" ></div>
      <input type="text" placeholder="Enter Brukernavn" name="brukernavn" onChange="valider_brukernavn()" required>

      <label for="password"><b>Password *</b></label>
      <div id = "feilPassword" class="inputLabel"></div>
      <input type="password" placeholder="Enter Password" name="password" onChange= "valider_pasword()" required>

      <label for="navn"><b>Full Navn *</b></label>
      <div id = "feilNavn" class="inputLabel"></div>
      <input type="text" placeholder="Enter Full Navn" name="navn" onChange = "valider_navn()" required>

      <div class="clearfix">
          <button type="submit" class="signupbtn" name="register" value="Register">Sign Up</button>
          <a class="cancelbtn" href="index.php" style="margin-top:7px;color:white;">Cancel</a>
      </div>
    </div>
  </form>
  <script type="text/javascript">
    function valider_brukernavn()
    {
        regEx = /^([a-åA-Å0-9])*$/;
        OK = regEx.test(document.registerform.brukernavn.value);
        if(!OK)
        {
            document.getElementById("feilbrukernavn").innerHTML="Feil: bruk uten mellom-rom.";
            return false;
        }
        document.getElementById("feilbrukernavn").innerHTML="";
        return true;
    }
    function valider_pasword()
    {
        regEx = /^([a-åA-Å0-9]{4,12})*$/;
        OK = regEx.test(document.registerform.password.value);
        if(!OK)
        {
            document.getElementById("feilPassword").innerHTML="Feil: kun 4-12 siffre.";
            return false;
        }
        document.getElementById("feilPassword").innerHTML="";
        return true;
    }
    function valider_navn()
    {
        regEx = /^([a-åA-Å ])*$/;
        OK = regEx.test(document.registerform.navn.value);
        if(!OK)
        {
            document.getElementById("feilNavn").innerHTML="Feil: prøv uten nummer.";
            return false;
        }
        document.getElementById("feilNavn").innerHTML="";
        return true;
    }
    </script>
</body>
</html>