<?php
class Person
{
    private $personnr;
    private $fornavn;
    private $etternavn;
    private $adresse;
    private $postnr;
    private $poststed;
    private $telefonnr;
    private $land;

    //Setter...
    public function setPersonnr($innPersonnr)
    {$this->personnr = $innPersonnr;}
    public function setFornavn($innFornavn)
    {$this->fornavn = $innFornavn;}
    public function setEtternavn($innEtternavn)
    {$this->etternavn = $innEtternavn;}
    public function setAdresse($innAdresse)
    {$this->adresse = $innAdresse;}
    public function setPostnr($innPostnr)
    {$this->postnr = $innPostnr;}
    public function setPoststed($innPoststed)
    {$this->poststed = $innPoststed;}
    public function setTelefonnr($innTelefonnr)
    {$this->telefonnr = $innTelefonnr;}
    public function setLand($land)
    {$this->land = $land;}

    //Getter...
    public function getPersonNr(){return $this->personnr;}
    public function getLand(){return $this->land;}

    public function insertIntoDatabase()
    {
        $db = new mysqli("localhost", "root", "", "vm_ski");
        if (!$db) {
            printError("Kunne ikke koble til databasen, prøv igjen."); //denne initierer en die(), sjekk printError funksjonen lenger opp i denne filen
        }
        $sql = "Insert into Person (Personnr, Fornavn, Etternavn, Adresse, PostNr, Poststed, Telefonnr, Land) ";
        $sql .= "values('$this->personnr',
                        '$this->fornavn',
                        '$this->etternavn',
                        '$this->adresse',
                        '$this->postnr',
                        '$this->poststed',
                        '$this->telefonnr',
                        '$this->land')";

        $result = $db->query($sql);
        if ($result) {
            echo "* Person med personnummer: $this->personnr ble lagret!";
        } else {
            echo ("SQL: " . $sql . ", Databasefeil i Person: " . $db->error);
        }
        $db->close();
    }

    public function getPersonList()
    {
        $nameArr = array();
        $db = new mysqli("localhost", "root", "", "vm_ski");
        if (!$db) {
            printError("Kunne ikke koble til databasen, prøv igjen."); //denne initierer en die(), sjekk printError funksjonen lenger opp i denne filen
        }
        $sql = "Select Fornavn,Etternavn from Person";
        $result = $db->query($sql);
        if ($result) {
            $antallRader = $db->affected_rows;
            for ($i = 0; $i < $antallRader; $i++) {
                $radObject = $result->fetch_object();
                $nameArr[] = $radObject->Etternavn . "," . $radObject->Fornavn;
            }
        } else {
            echo ("SQL: " . $sql . ", Databasefeil i Person: " . $db->error);
        }
        return $nameArr;
        $db->close();
    }
    public static function getPersonFromNavn($strFullNavn)
    {
        $selectedPerson = new Person();
        $separated = explode(",", $strFullNavn);
        $db = new mysqli("localhost", "root", "", "vm_ski");
        if (!$db) {
            printError("Kunne ikke koble til databasen, prøv igjen."); //denne initierer en die(), sjekk printError funksjonen lenger opp i denne filen
        }
        $sql = "Select * from Person WHERE Etternavn = '$separated[0]' AND Fornavn= '$separated[1]'";
        $result = $db->query($sql);
        if ($result) {
            $antallRader = $db->affected_rows;
            if ($antallRader > 0) {
                $radObject = $result->fetch_object();
                $selectedPerson->setPersonnr($radObject->Personnr);
                $selectedPerson->setFornavn($radObject->Fornavn);
                $selectedPerson->setEtternavn($radObject->Etternavn);
                $selectedPerson->setAdresse($radObject->Adresse);
                $selectedPerson->setPostnr($radObject->PostNr);
                $selectedPerson->setPoststed($radObject->Poststed);
                $selectedPerson->setTelefonnr($radObject->Telefonnr);
                $selectedPerson->setLand($radObject->Land);
            }
        } else {
            echo ("SQL: " . $sql . ", Databasefeil i Person: " . $db->error);
        }
        $db->close();
        return $selectedPerson;
    }

}
