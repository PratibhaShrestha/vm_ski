<?php
class Ovelse
{
    private $ovelsenr;
    private $type;
    private $sted;
    private $dato;
    private $tid;

    public function setOvelsenr($ovelsenr)
    {$this->ovelsenr = $ovelsenr;}
    public function setType($type)
    {$this->type = $type;}
    public function setSted($sted)
    {$this->sted = $sted;}
    public function setDato($dato)
    {$this->dato = $dato;}
    public function setTid($tid)
    {$this->tid = $tid;}

    public function insertIntoDatabase()
    {
        $db = new mysqli("localhost", "root", "", "vm_ski");
        if (!$db) {
            printError("Kunne ikke koble til databasen, prøv igjen."); //denne initierer en die(), sjekk printError funksjonen lenger opp i denne filen
        }
        $db->set_charset("utf8");
        $sql = "Insert into Ovelse (OvelseNr,Type,Sted,Dato,Tid) ";
        $sql .= "values('$this->ovelsenr',
                        '$this->type',
                        '$this->sted',
                        '$this->dato',
                        '$this->tid')";

        $result = $db->query($sql);
        if ($result) {
            echo "* Ovelse med ovelsenr: $this->ovelsenr ble lagret!";
        } else {
            echo ("SQL: " . $sql . ", Databasefeil i Ovelse: " . $db->error);
        }
        $db->close();
    }
    public function updateDatabase()
    {
        $db = new mysqli("localhost", "root", "", "vm_ski");
        if (!$db) {
            printError("Kunne ikke koble til databasen, prøv igjen."); //denne initierer en die(), sjekk printError funksjonen lenger opp i denne filen
        }
        $db->set_charset("utf8");
        $sql = "Update Ovelse SET ";
        $sql .= "Type='$this->type',
                Sted='$this->sted',
                Dato='$this->dato',
                Tid='$this->tid'";
        $sql .= " WHERE OvelseNr='$this->ovelsenr'";

        $result = $db->query($sql);
        if ($result) {
            echo "* Ovelse med ovelsenr: $this->ovelsenr ble Oppdatert!";
        } else {
            echo ("SQL: " . $sql . ", Databasefeil i Ovelse: " . $db->error);
        }
        $db->close();
    }

    public function getOvelseList()
    {
        $typeArr = array();
        $db = new mysqli("localhost", "root", "", "vm_ski");
        if (!$db) {
            printError("Kunne ikke koble til databasen, prøv igjen."); //denne initierer en die(), sjekk printError funksjonen lenger opp i denne filen
        }
        $sql = "Select Type from Ovelse";
        $result = $db->query($sql);
        if ($result) {
            $antallRader = $db->affected_rows;
            for ($i = 0; $i < $antallRader; $i++) {
                $radObject = $result->fetch_object();
                $typeArr[] = $radObject->Type;
            }
        } else {
            echo ("SQL: " . $sql . ", Databasefeil i Ovelse: " . $db->error);
        }
        $db->close();
        return $typeArr;
    }

    public static function getOvelseNrFromType($strType)
    {
        $selectedOvelsenr = "0";
        $db = new mysqli("localhost", "root", "", "vm_ski");
        if (!$db) {
            printError("Kunne ikke koble til databasen, prøv igjen."); //denne initierer en die(), sjekk printError funksjonen lenger opp i denne filen
        }
        $sql = "Select OvelseNr from Ovelse WHERE Type = '$strType'";
        $result = $db->query($sql);
        if ($result) {
            $antallRader = $db->affected_rows;
            if ($antallRader > 0) {
                $radObject = $result->fetch_object();
                $selectedOvelsenr = $radObject->OvelseNr;
            }
        } else {
            echo ("SQL: " . $sql . ", Databasefeil i Ovelse: " . $db->error);
        }
        $db->close();
        return $selectedOvelsenr;
    }

}
