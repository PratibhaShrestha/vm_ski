<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html;" charset=UTF-8">
	<title>Show Personer</title>
    <link rel="shortcut icon" href="../../favicon.ico">
	<link rel="stylesheet" type="text/css" href="css/normalize.css" />
	<link rel="stylesheet" type="text/css" href="css/demo.css" />
	<link rel="stylesheet" type="text/css" href="css/icons.css" />
    <link rel="stylesheet" type="text/css" href="css/style2.css" />

    <script src="js/modernizr.custom.js"></script>
    <script type="text/javascript" src="./forms/view.js"></script>
	<link rel="stylesheet" type="text/css" href="./forms/view.css" media="all">
    <link rel="stylesheet" type="text/css" href="style.css" media="all">
    <script defer src="fonts/solid.js" integrity="sha384-+Ga2s7YBbhOD6nie0DzrZpJes+b2K1xkpKxTFFcx59QmVPaSA8c7pycsNaFwUK6l" crossorigin="anonymous"></script>
    <script defer src="fonts/fontawesome.js" integrity="sha384-7ox8Q2yzO/uWircfojVuCQOZl+ZZBg2D2J5nkpLqzH1HY0C1dHlTKIbpRz/LG23c" crossorigin="anonymous"></script>

</head>

<body id="main_body">
<div id="show_container">
			<div class="form_description">
                <p></p>
				<h2 class="show_title"><i class="fas fa-calendar fa-2x"></i> &nbsp;Alle Øvelser-Utøver-Publikum liste ...</h2>
				<p></p>
            </div>

        <table class="show_table">
            <tr>
            <th>Øvelse</th>
            <th>Person</th>
            <th>Deltakelse Type</th>
            </tr>

<?php
$db = new mysqli("localhost", "root", "", "vm_ski");
$db->set_charset("utf8");
$sql = "SELECT Person.Fornavn AS Person, Ovelse.Type AS Ovelse,Registrasjon.Type AS DType FROM Person INNER JOIN(Registrasjon INNER JOIN Ovelse ON Registrasjon.OvelseNr= Ovelse.OvelseNr) ON Person.Personnr= Registrasjon.PersonNr";
$resultat = $db->query($sql);
if (!$resultat) {
    echo "Feil med lesing fra database";
}
print_r($resultat);

while ($rad = $resultat->fetch_array(MYSQLI_ASSOC)) {
    ?>
            <tr>
            <td><?echo $rad["Ovelse"] ?></td>
            <td><?echo $rad["Person"] ?></td>
            <td><?echo $rad["DType"] ?></td>
            </tr>
<?php
}
$db->close();
?>
</table>
</div>
<nav id="bt-menu" class="bt-menu">
				<a href="#" class="bt-menu-trigger"><span>Menu</span></a>
				<ul>
                    <li><a href="./index.php" class="bt-icon icon-home"></a>Home</li>
					<li><a href="./forms/person.php" class="bt-icon icon-user-outline"></a>Register Person</li>
					<li><a href="./forms/ovelse.php" class="bt-icon icon-flag"></a>Register Øvelse</li>
					<li><a href="./forms/registration.php" class="bt-icon icon-file-add"></a>Register Deltakelse</li>
					<li><a href="./ShowOvelse.php" class="bt-icon icon-calendar"></a>Show Øvelse liste</li>
					<li><a href="./ShowPerson.php" class="bt-icon icon-browser" ></a>Show Person Liste</li>
				</ul>
			</nav>
</body>

<script src="./js/classie.js"></script>
<script src="./js/borderMenu.js"></script>
</html>