<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>VM ski 2018</title>
		<link rel="shortcut icon" href="../favicon.ico">
		<link rel="stylesheet" type="text/css" href="css/normalize.css" />
		<link rel="stylesheet" type="text/css" href="css/demo.css" />
		<link rel="stylesheet" type="text/css" href="css/icons.css" />
		<link rel="stylesheet" type="text/css" href="css/style2.css" />
		<link rel="stylesheet" type="text/css" href="css/login.css">
		<script src="js/modernizr.custom.js"></script>
	</head>
	<?php
	session_start();
	?>
	<body>
	 <?php
		if( isset($_SESSION["logged_in"]) and isset($_SESSION["user_name"]) ){
			$isLoggedIn = $_SESSION["logged_in"];
			$user_name = $_SESSION["user_name"];
			if(empty($user_name) or $isLoggedIn != 1){
				$_GLOBALS["allowRegister"] = false;
			}else{
				$_GLOBALS["allowRegister"] = true;
				echo "<div style='position: absolute; right:0; margin:20px;'>";
				echo "<span>Logged in user: $user_name !!!</span>";
				echo "<br/>";
				echo "<a id='logoutbtn' style='display:block;margin:5px;padding:3px;background: #cc4a58; color:white; text-align:center;' href='logout.php'>logout</a>";
				echo "</div>";
			}
		}
	 ?>
		<div class="container">
			<header class="codrops-header">
				<h1>VM SKI 2018</h1>
				<img src="./forms/ski.png"/>
				<nav class="codrops-demos">
					<a href="./forms/person.php">Register Person</a>
					<a href="./forms/ovelse.php">Register Øvelse</a>
					<a href="./forms/registration.php">Entry</a>
					<a href="./ShowOvelse.php">Show Øvelse Liste</a>
					<a href="./ShowPerson.php">Show Person Liste</a>
					<a href="./ShowUtoverOvelsePublikum.php">Show Øvelse-Deltagelse Liste</a>

					<?php
					if( isset($_GLOBALS["allowRegister"]) && $_GLOBALS["allowRegister"] == true){
						echo "<a href='register.php'>Register New Admin</a>";
					}
					?>
				</nav>
			</header>
			<nav id="bt-menu" class="bt-menu">
				<a href="#" class="bt-menu-trigger"><span>Menu</span></a>
				<ul>
					<li><a href="./forms/person.php" class="bt-icon icon-user-outline"></a>Register Person</li>
					<li><a href="./forms/ovelse.php" class="bt-icon icon-flag" ></a>Register Øvelse</li>
					<li><a href="./forms/registration.php" class="bt-icon icon-file-add" ></a>Register Deltakelse</li>
					<li><a href="./ShowOvelse.php" class="bt-icon icon-calendar" ></a>Show Øvelse liste</li>
					<li><a href="./ShowPerson.php" class="bt-icon icon-browser" ></a>Show Person Liste</li>
					<li><a href="./ShowUtoverOvelsePublikum.php" class="bt-icon icon-browser" ></a>Show Øvelse-Deltagelse Liste</li>
				</ul>
			</nav>
		</div><!-- /container -->
	</body>
	<script src="js/classie.js"></script>
	<script src="js/borderMenu.js"></script>
</html>
